ay = "ay"
way = "way"
consonant = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "z"]
vowel = ["a", "e", "i", "o", "u", "y"]
pig_latin_string = ""
user_sentence = input("Enter a sentence to translate to Pig Latin: ")
words = user_sentence.split()
for user_word in words:
    first_letter = user_word[0]
    first_letter = str(first_letter)
    if first_letter in consonant:
        length_of_word = len(user_word)
        remove_first_letter = user_word[1:length_of_word]
        pig_latin = remove_first_letter + first_letter + ay
        pig_latin_string = pig_latin_string + " " + pig_latin
    elif first_letter in vowel:
        pig_latin = user_word + way
        pig_latin_string = pig_latin_string + " " + pig_latin
    else:
        print("Enter a correct sentence using only words!")
print(pig_latin_string)
